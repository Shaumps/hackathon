from bottle import Bottle, static_file, HTTPError, request
import cx_Oracle
import pandas as pd
import numpy as np
from helper.filter import get_clearance, get_filtered_fault_data, get_tail_numbers


app = Bottle(__name__)

# Set up server connection

connection = cx_Oracle.connect('team4', 'M0Dh4ckath0n!!', '132.145.62.163:1521/wbp.sub03271103252.vcn0327110325.oraclevcn.com')

@app.route('/')
def homepage():
	 return static_file('index.html', root='frontend/views')


@app.route('/code/<filepath:path>')
def serve_static_code(filepath):
	 return static_file(filepath, root='frontend/code')


@app.route('/views/<filepath:path>')
def serve_static_view(filepath):
	 return static_file(filepath, root='frontend/views')

@app.get('/api/get_names')
def get_names():
	 query = """SELECT FIRST_NAME, SURNAME FROM DATA_USERS"""
	 df = pd.read_sql(query, con=connection)
	 return {'names': list((df['FIRST_NAME'] + df['SURNAME']).sort_values().values)}


@app.post('/api/get_named_permissions')
def get_named_permissions():
	 name = request.json
	 firstname, surname = name['name'].split(' ')
	 df = get_clearance(surname.lower().replace(' ', ''), firstname.lower().replace(' ', ''), connection)

	 # assumes get_clearance only returns one name
	 clearance = df['SECURITY_CLEARENCE'].str.lower().str.replace(' ', '').values
	 itar = df['TAA_REGSITERED'].str.lower().str.replace(' ', '').values
	 country = df['INTERNATIONAL_ACCESS'].str.lower().str.replace(' ', '').values
	 organisation = df['ORGANISATION'].str.lower().str.replace(' ', '').values

	 return {
	 	'clearance': list(clearance)[0], 
	 	'itar': list(itar)[0],
	 	'country': list(country)[0],
	 	'organisation': list(organisation)[0]
 	}


@app.post('/api/get_filtered_data')
def get_filtered_data():
	 permissions = request.json
	 df = get_filtered_fault_data(permissions['clearance'], permissions['itar'], permissions['country'], permissions['organisation'], connection)

	 df = df[['DATE_OF_OCCURRENCE', 'REGISTRATION', 'MAJOR_SYSTEM_1', 'OPERATION_NAME', 'SHIP_STATION']]
	 df['DATE_OF_OCCURRENCE'] = pd.to_datetime(df['DATE_OF_OCCURRENCE']).dt.strftime('%Y-%m-%d')

	 return df[['DATE_OF_OCCURRENCE', 'REGISTRATION', 'MAJOR_SYSTEM_1', 'OPERATION_NAME', 'SHIP_STATION']].head(100).T.to_json()

@app.post('/api/get_tail_no')
def get_tail_no():
	 permissions = request.json
	 tails = get_tail_numbers(permissions['country'], connection)

	 return {'tailNumbers': list(tails)}

@app.get('/api/noddy_data')
def get_data():
	return {
		'dataObjects': [
			{
			'x': [1, 2, 3, 4, 5],
			'y': [1, 4, 9, 16, 25]
			}
		]
	}

@app.post('/api/get_data_with_params')
def get_data_with_params():
	params = request.json

	params['status'] = 200

	return params

@app.post('/api/get_tail_flight_data')
def get_flight_hour_data():
	params = request.json
	query="""SELECT TAIL_NO, SORTIE_DURATION, TAKE_OFF_DATE FROM C130J_SORTIES WHERE TAIL_NO = '{0}'""".format(params['tailNo'])
	sorties = pd.read_sql(query, con=connection)

	# Dasor table
	query = """SELECT REGISTRATION, DATE_REPORTED FROM DASOR WHERE REGISTRATION = '{0}' ORDER BY DATE_REPORTED DESC""".format(params['tailNo'])
	dasor = pd.read_sql(query, con=connection)

	if (sorties.shape[0] > 0):
		 failDates = dasor['DATE_REPORTED']
		 flyDuration = sorties['SORTIE_DURATION']
		 hourMin = pd.DatetimeIndex(flyDuration)
		 flyDuration = hourMin.hour * 60  + hourMin.minute
		 flyDurationCum = flyDuration.values.cumsum()

		 # Zero cumsum whenever when failure
		 # print(failDates.values)
	if failDates.shape[0] > 0:
		for zeroCount in range(failDates.shape[0]):
			# sortiesThisTail['TAKE_OFF_DATE'].values.min()
			# sortiesThisTail['TAKE_OFF_DATE'].values.max()
			if (sorties['TAKE_OFF_DATE'].values.min() < failDates.values[zeroCount] and sorties['TAKE_OFF_DATE'].values.max() > failDates.values[zeroCount]):
				test = sorties['TAKE_OFF_DATE'].values<failDates.values[zeroCount]
				test = min(np.where(test)[0])

				flyDurationCum[test:] = flyDurationCum[test:] - flyDurationCum[test]

	return {'x': [i for i in range(len(flyDurationCum))], 'y': flyDurationCum.tolist()}

	 	 	 	 	 	 	 	 	 # if test.shape[0]>0:
	 	 	 	 	 	 	 	 	 #	    for zeroVal in test:
	 	 	 	 	 	 	 	 	 #	 	 	    flyDurationCum[zeroVal:] = flyDurationCum[zeroVal:] - flyDurationCum[zeroVal]


if __name__ == '__main__':
	 app.run(host='localhost', port=8080, debug=True)
