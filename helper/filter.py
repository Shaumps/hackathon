import pandas as pd

def get_clearance(surname, firstname, connection):
    query = """SELECT SURNAME, FIRST_NAME, ORGANISATION, SECURITY_CLEARENCE, TAA_REGSITERED, INTERNATIONAL_ACCESS FROM DATA_USERS"""
    df = pd.read_sql(query, con=connection)

    df = df[df['SURNAME'].str.lower().str.replace(' ', '') == surname]
    df = df[df['FIRST_NAME'].str.lower().str.replace(' ', '') == firstname]
    return df


def get_filtered_fault_data(clearance, itar, country, organisation, connection):
    query = """SELECT * FROM DASOR WHERE MK = 'C130J'
    ORDER BY DATE_OF_OCCURRENCE DESC
    FETCH NEXT 100 ROWS ONLY
    """
    df = pd.read_sql(query, con=connection)

    mil = ['othernationus', 'othernationfr', 'othernation5eyes', 'othernationnonnato', 'othernationnato', 'rafunit', 'rafcommand', 'de&spt', 'militaryaviationauthority', 'othergovernmentdepartment']

    if itar == 'notitarcleared':
        df = df[df['MAJOR_SYSTEM_1'].str.lower().str.replace(' ', '') != 'engine']

    if country != 'ukonly':
        df = df[df['UNIT_SQUADRON'].str.lower().str.replace(' ', '') != 'ablock']
        df = df[~df['SHIP_STATION'].str.lower().str.replace(' ', '').str.contains('ooa')]

    if clearance != 'dv':
        df = df[df['UNIT_SQUADRON'].str.lower().str.replace(' ', '') != 'ablock']
        df = df[~df['SHIP_STATION'].str.lower().str.replace(' ', '').str.contains('ooa')]

    if organisation not in mil:
        df = df[df['UNIT_SQUADRON'].str.lower().str.replace(' ', '') != 'ablock']
        df = df[~df['SHIP_STATION'].str.lower().str.replace(' ', '').str.contains('ooa')]
        df = df[df['COUNTRY'].str.lower().str.replace(' ', '') != 'afghanistan']
        df = df[df['COUNTRY'].str.lower().str.replace(' ', '') != 'estonia']
        df = df[~df['OPERATION_NAME'].str.lower().str.replace(' ', '').str.contains('shader')]
        df = df[~df['OPERATION_NAME'].str.lower().str.replace(' ', '').str.contains('zorro')]

    if clearance != 'dv' or clearance != 'sc':
        df = df[df['COUNTRY'].str.lower().str.replace(' ', '') != 'afghanistan']
        df = df[df['COUNTRY'].str.lower().str.replace(' ', '') != 'estonia']

    if country == 'fronly' or country == 'usonly':
        df = df[~df['OPERATION_NAME'].str.lower().str.replace(' ', '').str.contains('shader')]

    if country != 'usonly' and country != 'ukonly':
        df = df[~df['OPERATION_NAME'].str.lower().str.replace(' ', '').str.contains('zorro')]

    return df


def get_tail_numbers(country, connection):

    if country == 'nato':
        query = """SELECT C130_FLYING_RECORDS.TAIL_NO FROM C130_FLYING_RECORDS UNION ALL
                SELECT FR_C130_FLYING_RECORDS.TAIL_NO FROM FR_C130_FLYING_RECORDS UNION ALL
                SELECT US_C130_FLYING_RECORDS.TAIL_NO FROM US_C130_FLYING_RECORDS"""

    elif country == 'ukonly':
        query = """SELECT TAIL_NO FROM C130_FLYING_RECORDS"""

    elif country == 'usonly':
        query = """SELECT TAIL_NO FROM US_C130_FLYING_RECORDS"""

    elif country == 'fronly':
        query = """SELECT TAIL_NO FROM FR_C130_FLYING_RECORDS"""

    df = pd.read_sql(query, con=connection)

    return df['TAIL_NO'].unique()