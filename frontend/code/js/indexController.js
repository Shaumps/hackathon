'use strict';

angular.module('hackApp')
    .controller('IndexController', ['$scope', 'IndexService', function($scope, IndexService){

    var indexService;

    var submitName = function() {
        var selectedName = $scope.selectedName;
        $scope.loading = true;
        $scope.permissions = ': Loading permissions...';
        $scope.fault_rows = [];
        $scope.tailNumbers = [];

        indexService.getPermissionsFromName(selectedName).then(function(success) {
            $scope.permissions = ': ' + success.clearance + ', ' + success.itar + ', ' + success.country + ', ' + success.organisation;

            indexService.getFilteredFaultData(success.clearance, success.itar, success.country, success.organisation).then(function(success) {
                $scope.fault_rows = success;
                $scope.loading = false;
            });

            indexService.getTailNumbers(success.country).then(function(success) {
                $scope.tailNumbers = success.tailNumbers;
                $scope.loadedFlightNumbers = true;
            });
        });

    };

    var selectTailNumber = function() {
        var selectedTailNo = $scope.selectedTailNumber;

        indexService.getHoursFromTailNumber(selectedTailNo).then(function(success) {
            Plotly.plot( document.getElementById("plot"), [{
            x: success.x,
            y: success.y }],
            {
                title: 'Cumulative Flight Minutes Between Maintainance'
            }
        );
        });
    };

    var init = function(){
        indexService = IndexService;
        $scope.loading = true;
        $scope.selectName = submitName;
        $scope.tailNumbers = [];
        $scope.loadedFlightNumbers = false;
        $scope.names = [];
        $scope.fault_rows = [];
        $scope.selectTailNumber = selectTailNumber;

        indexService.getNames(function(error){})
            .then(
                function(success) {
                    $scope.names = success;
                    $scope.loading = false;
                }
            );
    };

    init();
    }]);
