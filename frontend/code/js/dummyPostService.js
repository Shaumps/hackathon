'use strict';

angular.module('hackApp').factory('DummyPostService', ['$http', function($http){
	var service = {
	}

	service.PostForData = function(param, to, post) {
		var payload = {
			paramName: param,
			toName: to,
			postName: post
		};

		$http.post('/api/get_data_with_params', payload).then(
			function(success) {
				return success.data
			})
	};

	return service;
}]);