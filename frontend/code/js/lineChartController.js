'use strict';

angular.module('hackApp').controller('LineChartController', ['$scope', 'LineChartService', function($scope, LineChartService) {
	var lineChartService;

	var init = function() {
		lineChartService = LineChartService;

		Plotly.plot( document.getElementById("plot"), [{
		    x: [],
		    y: [] }],
		    {
		    	title: 'This is a test plot'
		    }
	    );

	    lineChartService.get_noddy_data().then(
	    	function(success) {
	    		Plotly.plot( document.getElementById("plot"), success,
				    {
				    	title: 'This is a test plot (now with data)'
				    }
			    );
	    	})
	};

	init();
}]);