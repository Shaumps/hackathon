'use strict';

angular.module('hackApp').factory('LineChartService', ['$http', function($http) {
	var service = {
		data: {}
	};

	service.get_noddy_data = function(){
		return $http.get('/api/noddy_data').then(
			function(success) {
				return success.data.dataObjects;
			});
	};

	return service;
}]);