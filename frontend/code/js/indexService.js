'use strict';

angular.module('hackApp')
    .factory('IndexService', ['$http', '$log', function($http, $log){
        var service = {
            model: {
                names: []
            }
        }
        service.getNames = function(errorCallback) {
            return $http.get('/api/get_names')
                .then(
                    function(success) {
                        return success.data.names;
                    },
                    function(error) {
                        errorCallback(error);
                    }
                );
        };

        service.getPermissionsFromName = function(selectedName) {
            var payload = {
                name: selectedName
            };

            return $http.post('/api/get_named_permissions', payload).then(
                function(success) {
                    return success.data
                });
        };

        service.getTailNumbers = function(countryIn) {
            var payload = {
                country: countryIn
            };

            return $http.post('/api/get_tail_no', payload).then(function(success) {
                return success.data;
            });
        };

        service.getHoursFromTailNumber = function(selectedTailNumber) {
            var payload = {
                tailNo: selectedTailNumber
            };

            return $http.post('/api/get_tail_flight_data', payload).then(function(success) {
                return success.data;
            });
        };

        service.getFilteredFaultData = function(clearanceIn, itarIn, countryIn, organisationIn) {
            var payload = {
                clearance: clearanceIn,
                itar: itarIn,
                country: countryIn,
                organisation: organisationIn
            };

            return $http.post('/api/get_filtered_data', payload).then(
                function(success) {
                    return success.data;
                });
        };

    return service;
    }]);
